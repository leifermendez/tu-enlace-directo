import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

const dataTerms = require('../../../assets/data/terms-policy.json');

@Component({
  selector: 'app-page-text',
  templateUrl: './page-text.component.html',
  styleUrls: ['./page-text.component.scss']
})
export class PageTextComponent implements OnInit {
  public slug!: string;
  public itemSelec: string = 'service';
  public data: any;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {

    this.route.params.subscribe(res => {
      this.slug = res?.slug
      this.loadData()
    });
  }

  loadData(): void {
    this.data = dataTerms[this.slug]
  }

  scrollToMyElement(item: any) {
    this.itemSelec = item;
    var element = document.getElementById(item) as HTMLElement;
    element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    });
  }

}
