import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

const dataProducts = require('../../../../../assets/data/products.json');

@Component({
  selector: 'app-section-products',
  templateUrl: './section-products.component.html',
  styleUrls: ['./section-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SectionProductsComponent implements OnInit {

  public listProducts: any = [];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: [ '<img src="assets/images/productos/prev-carousel.svg" alt="" srcset="">', '<img src="assets/images/productos/next-carousel.svg" alt="" srcset="">"' ],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
  
  constructor() {
    dataProducts.forEach((element: any) => {
      this.listProducts.push(element);
    });
   }

  ngOnInit(): void {
  }

}
