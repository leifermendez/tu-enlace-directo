import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionCarouselUpComponent } from './section-carousel-up.component';

describe('SectionCarouselUpComponent', () => {
  let component: SectionCarouselUpComponent;
  let fixture: ComponentFixture<SectionCarouselUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionCarouselUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionCarouselUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
