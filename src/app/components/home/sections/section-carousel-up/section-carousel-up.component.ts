import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-section-carousel-up',
  templateUrl: './section-carousel-up.component.html',
  styleUrls: ['./section-carousel-up.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SectionCarouselUpComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: [ '<img src="assets/images/productos/prev-carousel.svg" alt="" srcset="">', '<img src="assets/images/productos/next-carousel.svg" alt="" srcset="">' ],
    responsive: {
      0: {
        items: 1
      },
    },
    nav: true
  }
  
  constructor() { }

  ngOnInit(): void {
  }

  goToIntagram(){
    window.open("https://www.instagram.com/tuenlacedirecto/", "_blank");
  }

  goToFacebook(){
    window.open("https://www.facebook.com/tuenlacedirecto", "_blank");
  }

  goToWhatsapp(){
    window.open("https://wa.me/message/5UFIFJHIHUBTO1", "_blank");
  }

  goToMail(){ 
    window.location.href = "mailto:info@tuenlacedirecto.com";
  }

}
