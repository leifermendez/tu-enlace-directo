import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../../../modal/modal.component';

const dataServices = require('../../../../../assets/data/services.json');

@Component({
  selector: 'app-section-services',
  templateUrl: './section-services.component.html',
  styleUrls: ['./section-services.component.scss']
})
export class SectionServicesComponent implements OnInit {

  public bsModalRef: BsModalRef | undefined;
  public listService: any = [];
  
  constructor(private modalService: BsModalService) { 
    dataServices.forEach((element: any) => {
      this.listService.push(element);
    });
  }

  ngOnInit(): void {
  }

  openModalWithComponent(service: any) {
    const initialState = {
      service
    }
    this.bsModalRef = this.modalService.show(ModalComponent, {initialState, class: 'modal-lg'});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
