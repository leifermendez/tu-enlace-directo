import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-contact',
  templateUrl: './section-contact.component.html',
  styleUrls: ['./section-contact.component.scss']
})
export class SectionContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  goToIntagram(){
    window.open("https://www.instagram.com/tuenlacedirecto/", "_blank");
  }

  goToFacebook(){
    window.open("https://www.facebook.com/tuenlacedirecto", "_blank");
  }

  goToWhatsapp(){
    window.open("https://wa.me/message/5UFIFJHIHUBTO1", "_blank");
  }

  goToMail(){ 
    window.location.href = "mailto:info@tuenlacedirecto.com";
  }
}
