import {Component, EventEmitter, OnInit, Output, Input, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit , AfterViewInit{

  @Input() itemSelec: string = '';
  @Output() scrollToMyElement = new EventEmitter();
  @Input() mode = false

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      const item = this.route.snapshot.queryParamMap.get('section')
      if(item){
        this.onScrollToMyElement(item)
      }
    },500)
  }


  onScrollToMyElement(item: any) {
    if (this.mode) {
      this.router.navigate(['/'], {queryParams: {section: item}})
    } else {
      this.itemSelec = item;
      this.scrollToMyElement.emit(item);
    }

  }

}
