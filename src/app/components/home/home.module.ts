import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { SectionVideoComponent } from './sections/section-video/section-video.component';
import { HeaderComponent } from './header/header.component';
import { SectionServicesComponent } from './sections/section-services/section-services.component';
import { SectionProductsComponent } from './sections/section-products/section-products.component';
import { SectionContactComponent } from './sections/section-contact/section-contact.component';
import { FooterComponent } from './footer/footer.component';
import { SectionCarouselUpComponent } from './sections/section-carousel-up/section-carousel-up.component';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SectionCarouselUpComponent,
    SectionVideoComponent,
    SectionServicesComponent,
    SectionProductsComponent,
    SectionContactComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    HomeRoutingModule,
    ModalModule.forRoot(),
    CarouselModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SectionContactComponent
  ],
  providers: []
})
export class HomeModule { }
