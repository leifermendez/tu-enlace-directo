import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  public itemSelec: string = 'service';
  
  constructor() {}

  ngOnInit(): void {
  }

  scrollToMyElement(item: any) {
    this.itemSelec = item;
    var element = document.getElementById(item) as HTMLElement;
    element.scrollIntoView({
      behavior: 'smooth',      
      block: 'start',
      inline: 'nearest'
    });
  }

}
